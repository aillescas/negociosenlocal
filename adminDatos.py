# coding: utf-8
import os
import logging 

import webapp2
import inspect

from google.appengine.ext.db.metadata import Kind
from django.template.loader import render_to_string
from google.appengine.ext import db
from google.appengine.api import memcache
from django.utils import simplejson


import modelos


modelosDefinidos = [clase for clase in dir(modelos) if inspect.isclass(eval("modelos." + clase)) and issubclass(eval("modelos." + clase), db.Model) and clase != "InfoLocal"]

class InfoLocalHandler(webapp2.RequestHandler):
    def get(self):
        
        infoLocal = modelos.getInfoLocal()
        if infoLocal:
            template_values = dict([[clave,eval("infoLocal." + clave)] for clave in modelos.InfoLocal.__dict__.keys() if (not clave.startswith("__") and not hasattr(eval("infoLocal." + clave),"__call__"))])

        else:
            template_values = {}
        
        modelosDB = Kind().all()
        modelosCreados = []

        for modelo in modelosDB:
            if not (modelo.kind_name.startswith("_") or modelo.kind_name == "InfoLocal"):
                modelosCreados.append(modelo.kind_name)

        resultado =  map(lambda x: [x, x in modelosCreados, [propiedad for propiedad in dir(eval("modelos." + x)) if (not propiedad.startswith("_")) and isinstance(eval("modelos." + x + "." + propiedad),db.Property) ]], modelosDefinidos)
        template_values.update({"modelos":resultado})

        path = os.path.join(os.path.dirname(__file__), 'html/infolocal.html')
        
        self.response.out.write(render_to_string(path, template_values))

        
    def post(self):
        infoLocal = modelos.getInfoLocal()
        if not infoLocal:
            infoLocal = modelos.InfoLocal()
        infoLocal.googleMapsKey   = self.request.get("mapsKey")
        infoLocal.title           = self.request.get("title")
        infoLocal.mailer          = self.request.get("email")
        latitud = self.request.get("latitud")
        longitud = self.request.get("longitud")
        infoLocal.latitudMapa     = float(latitud or "0.0")
        infoLocal.longitudMapa    = float(longitud or "0.0")
        infoLocal.stringLocalidad = self.request.get("localidad")

        numValoresMisc = int(self.request.get("numValoresMisc"))

        valoresMisc = [self.request.get("miscelanea"+str(indice)) for indice in range(1,numValoresMisc + 1) if self.request.get("miscelanea"+str(indice))]
        #infoLocal.miscelanea = [];
        infoLocal.miscelanea = valoresMisc
        logging.error("ValoresMiscelanea: " + str(infoLocal.miscelanea))           
        
        infoLocal.put()
        self.redirect("/")

class BorrarCacheAjaxHandler(webapp2.RequestHandler):
    def get(self):
        result = self.borrarMemCache()
        self.response.out.write(simplejson.dumps(result))
    def post(self):
        result = self.borrarMemCache()
        self.response.out.write(simplejson.dumps(result))

    @classmethod
    def borrarMemCache(cls):
        if memcache.flush_all():
            result = [{'resultado': 'Borrado realizado'}]
        else:
            result = [{'resultado': 'No ha sido posible el borrado'}]

        return result

class GuardarEntidadAjaxHandler(webapp2.RequestHandler):
    def get(self):

        if self.request.get("entidad"):
            self.almacenarEntidad()

        # Mostramos las entidades que hay en el modelo de datos

            
        template_values =  {"modelos":modelos}
        template_values.update(infoLocal.getMiscelanea())

        path = os.path.join(os.path.dirname(__file__), 'html/adminDatos.html')
        
        self.response.out.write(render_to_string(path, template_values))

    def post(self):
        
        if self.request.get("entidad"):
            self.almacenarEntidad()


    def almacenarEntidad(self):

        nombreEntidad = self.request.get("entidad")
        
        try:
            propiedades = [propiedad for propiedad in dir(eval("modelos." + nombreEntidad)) if (not propiedad.startswith("_")) and isinstance(eval("modelos." + nombreEntidad + "." + propiedad),db.Property) ]



            #entidad.nombre = "bbbb"
            diccionario = dict()
            for propiedad in propiedades:
                valorProp = self.request.get(nombreEntidad + propiedad)
                if valorProp:
                    #entidad.nombre = "aaa"
                    #Hay que convertir el valor string al tipo adecuado
                    if(isinstance(eval("modelos." + nombreEntidad + "." + propiedad), db.IntegerProperty)):
                        valorProp = int(valorProp)
                    elif(isinstance(eval("modelos." + nombreEntidad + "." + propiedad), db.FloatProperty)):
                        valorProp = float(valorProp)
                    diccionario[propiedad] = valorProp
                    
                    
            inicializacion = "".join(["," + str(clave) + '= diccionario["' + str(clave) + '"]' for clave in diccionario.keys()])[1:]
            entidad = eval("modelos."+ nombreEntidad + "(" + inicializacion + ")")                    
                    #setattr(entidad,propiedad,valorProp)
                    #logging.debug('Propiedad: ' + propiedad + " Valor: " + str(getattr(entidad,propiedad)))
                    #contenedor = valorProp
           
            entidad.put()
        
        except Exception as e:
            raise(e)
        result = [{"entidad": nombreEntidad}]
        self.response.out.write(simplejson.dumps(result))
            #logging.error("Error al recuperar los datos de la entidad: " + nombreEntidad + " " + str(e))
        

        #entidad.put()
