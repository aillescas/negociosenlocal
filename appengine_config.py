#!/usr/bin/env python
# coding: utf-8

import os
import re

from google.appengine.api import namespace_manager

def namespace_manager_default_namespace_for_request():
  """Determine which namespace is to be used for a request.

  """
  name = os.environ['SERVER_NAME']
  # Eliminamos la parte de www. y .com / .es / etc
  name = re.sub("^www\.","",name)
  
  division = re.split("\.",name)
  # Así mantenemos los subdominios dentro del namespace de los dominios
  name = division[len(division) - 2]

      
  return name

