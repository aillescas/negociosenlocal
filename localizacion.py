import logging
import webapp2

from django import template
from google.appengine.ext import db
from modelos import Negocio
from modelos import Usuario
from google.appengine.api import memcache
from django.utils import simplejson
from google.appengine.api import urlfetch

class LocalizarDireccionHandler(webapp2.RequestHandler):
    def get(self):
        direccion = self.request.get('direccion') + ",La linea de la concecpion"
        direccion = direccion.replace(" ","+")
        url = "http://maps.google.com/maps/api/geocode/json?address=%s&sensor=false" % direccion

        jsonresponse = urlfetch.fetch(url)

        datosLocalizacion = simplejson.loads(jsonresponse.content)
        
        if(datosLocalizacion["status"] != "OK"):
            self.response.out.write(str(datosLocalizacion))
        else:
            self.response.out.write(str(datosLocalizacion["results"][0]["geometry"]["location"]["lat"]) + " " + str(datosLocalizacion["results"][0]["geometry"]["location"]["lng"]))
