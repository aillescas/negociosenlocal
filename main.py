#!/usr/bin/env python
# coding: utf-8
#


import os
#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings

try:
    settings.configure(TEMPLATE_DEBUG=True,ROOT_PATH=os.path.dirname(__file__),TEMPLATE_DIRS=(os.path.dirname(__file__) + '/html',))
except RuntimeError:
    pass
#settings._target = None


import logging


import sesionUsuario
import webapp2

#from google.appengine.ext.webapp import util
from django.template.loader import render_to_string
from google.appengine.ext import db

#from google.appengine.api import namespace_manager

from registroUsuarios import RegistroUsuariosHandler
from registroUsuarios import RegistroUsuariosRequestHandler
from registroUsuarios import RegistroUsuariosConfirmacionHandler
from registroNegocios import RegistroNegociosHandler
from registroNegocios import RegistroNegociosRequestHandler
from localizacion     import LocalizarDireccionHandler
from modelos          import AjaxHandler
from principal        import PrincipalHandler
from modelos          import getInfoLocal
from modelos          import BorradoAjaxHandler
from adminDatos       import InfoLocalHandler 
from adminDatos       import GuardarEntidadAjaxHandler
from adminDatos       import BorrarCacheAjaxHandler
from sesionUsuario    import UsuarioAjaxHandler

templates = {"": "html/welcome.html", "servicios":"html/servicios.html", "contacto":"html/contacto.html", "legal":"html/avisoLegal.html"}
class MainHandler(webapp2.RequestHandler):
    def get(self):

        infoLocal = getInfoLocal()

        if not infoLocal:
            # Hay que blindar infoLocal para que sólo puedan acceder administradores
            self.redirect("/infoLocal")
            return


        logging.debug("Path: " + self.request.path[1:] + u" página: " + templates[self.request.path[1:]])
        
        #verificacionGoogle = '0wpPFUAMSVi_czabhIBrlRwdbRc-Iq2iQZiDl1yJnSE'
        template_values = infoLocal.getInfoDict()
        template_values.update(infoLocal.getMiscelanea())
        template_values.update(sesionUsuario.recuperarInformacionSesion(self.request))

        logging.debug("Valores pasados al template" + unicode(template_values))
        path = os.path.join(os.path.dirname(__file__), templates[self.request.path[1:]])

        self.response.out.write(render_to_string(path, template_values))




logging.getLogger().setLevel(logging.DEBUG)

application = webapp2.WSGIApplication([('/',               MainHandler),
                                       ('/principal',      PrincipalHandler),
                                       ('/registro',       RegistroUsuariosHandler),
                                       ('/registroPost',    RegistroUsuariosRequestHandler),
                                       ('/negocios',       RegistroNegociosHandler),
                                       ('/negociosPost',    RegistroNegociosRequestHandler),
                                       ('/ajax',           AjaxHandler),
                                       ('/infoLocal',      InfoLocalHandler),
                                       ('/admin/localizar',      LocalizarDireccionHandler),
                                       ('/admin/borrar',         BorradoAjaxHandler),
                                       ('/admin/guardarEntidad', GuardarEntidadAjaxHandler),
                                       ('/admin/borrarCache', BorrarCacheAjaxHandler),
                                       ('/confirmar',RegistroUsuariosConfirmacionHandler),
                                       ('/servicios',      MainHandler),
                                       ('/gestionUsuario', UsuarioAjaxHandler),
                                       ('/contacto', MainHandler),
                                       ('/legal', MainHandler)],
                                       debug=True)
                                      
                                      #    util.run_wsgi_app(application)


#if __name__ == '__main__':
#    main()

