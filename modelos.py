# coding: utf-8
import logging
import webapp2
import re
import random
import urllib

from operator import attrgetter

from google.appengine.api import memcache
from google.appengine.ext import db
from django.utils import simplejson


def getInfoLocal():
    return InfoLocal.all().get()


class InfoLocal(db.Model):
    googleMapsKey = db.StringProperty()
    title = db.StringProperty()
    mailer = db.StringProperty()
    latitudMapa = db.FloatProperty()
    longitudMapa = db.FloatProperty()
    stringLocalidad = db.StringProperty()
    miscelanea      = db.StringListProperty()

    def getInfoDict(self):
        return  {"title"     : self.title,
                 "key"       : self.googleMapsKey,
                 "mailer"    : self.mailer,
                 "latitud"   : self.latitudMapa,
                 "longitud"  : self.longitudMapa,
                 "localidad" : self.stringLocalidad}
        



    def getMiscelanea(self):
        
        if self.miscelanea:
            logging.debug("Valor de miscelanea: a" + str(self.miscelanea) + "a")
            return dict([re.split(":",pareja) for pareja in self.miscelanea])
        else:
            return dict()

        

class Usuario(db.Model):
    nombre = db.StringProperty(required=True)
    apellidos = db.StringProperty()
    correo = db.EmailProperty(required=True)
    #personalId = db.StringProperty(required=True)
    #idRefer = db.SelfReferenceProperty(indexed=False,collection_name="usuarios_referidos")
    password = db.StringProperty(indexed=False)
    confirmado = db.BooleanProperty(default=False)


class Iniciativa(db.Model):
    iniciador = db.ReferenceProperty(Usuario, required=True, collection_name="iniciativas")
    descripcion = db.TextProperty()
    
#class Cliente(db.Model):
#    idCliente = db.StringProperty()
#    nombre = db.StringProperty()
#    idRefer = db.ReferenceProperty(Usuario)
    #idTasa = db.ReferenceProperty(Tasa)

class UsuarioSinConfirmar(db.Model):
    codigoRecuperacion = db.StringProperty(required=True)
    usuario = db.ReferenceProperty(Usuario,required=True)

    @staticmethod
    def generarUsuario(usuario):
        codigo = str(random.random())[2:]
        usuarioSC = UsuarioSinConfirmar(codigoRecuperacion=codigo,usuario=usuario)
        return usuarioSC


class Sesion(db.Model):
    idSesion = db.StringProperty()
    usuario = db.ReferenceProperty(Usuario)
    inicio = db.DateTimeProperty(auto_now_add=True)

class Categoria(db.Model):
    nombre = db.StringProperty(required=True,)
    categoria_padre = db.SelfReferenceProperty(collection_name="categorias_hijas")
    orden = db.IntegerProperty(required=True)

    @classmethod
    def categoriasOrdenadas(cls):
        return [valor.nombre for valor in sorted(Categoria.all(),key=attrgetter('orden','nombre'))]



class Negocio(db.Model):

    nombre = db.StringProperty(required=True)
    direccion = db.StringProperty(required=True)
    #direccionAdicional = db.StringProperty()
    telefono = db.PhoneNumberProperty()
    movil = db.PhoneNumberProperty()
    fax = db.PhoneNumberProperty()
    web = db.StringProperty()
    email = db.EmailProperty()
    descripcion = db.StringProperty(indexed=False)
    latitud = db.FloatProperty(required=True)
    longitud = db.FloatProperty(required=True)
    #categoria = db.StringProperty()
    categoria = db.ReferenceProperty(Categoria,required=True,collection_name="negocios_categoria")
    logoPath = db.StringProperty(indexed=False)
    
    #propietario = db.ReferenceProperty(Usuario,collection_name="propiedades")
    idRefer = db.ReferenceProperty(Usuario,collection_name="negocios_referidos")

class BINdbFile(db.Model):
    nombre      = db.StringProperty(required=True)
    cuerpo      = db.BlobProperty(required=True)
    contentType = db.StringProperty(required=True)

class Producto(db.Model):
    cliente = db.ReferenceProperty(Negocio)
    precio = db.FloatProperty()
    descripcion = db.StringProperty()
    inicio = db.DateTimeProperty(auto_now_add=True)
    fin = db.DateTimeProperty()

class BorradoAjaxHandler(webapp2.RequestHandler):
    def get(self):
        self.BorradoEntidad()

    def put(self):
        self.BorradoEntidad()

    def BorradoEntidad(self):

        entidad = urllib.unquote(self.request.get('entidad'))
        logging.debug("Entidad: " + entidad)
        
        if entidad:
            entidad = entidad.title()
            consulta = db.Query(eval(entidad),True)
            valores = consulta.fetch(100)
            db.delete(valores)
            valores = consulta.count()
            result = [{"cuenta":valores,
                       "entidad":entidad}]
            
            self.response.out.write(simplejson.dumps(result))

        return 
            

            
    
class AjaxHandler(webapp2.RequestHandler):
    def get(self):
        self.respuestaAjax()

    def put(self):
        self.respuestaAjax()

    def respuestaAjax(self):
        ultimo = self.request.get('ultimo')
        categoria = urllib.unquote(self.request.get('categoria'))
        
        valoresCache = memcache.get(categoria + ultimo)
        logging.debug("Key: " + categoria + ultimo + " Valores Cache " + str(valoresCache))
        if not valoresCache:

            if categoria:
                logging.debug("Categoria: " + categoria)
                categoriaAsociada = Categoria.all().filter('nombre = ',categoria).get()
                if categoriaAsociada:
                    consultaNegocios = categoriaAsociada.negocios_categoria
                else:
                    #No hay categoria hay que devolver una lista vacía
                    logging.debug(u"No existe la categoría")
                    self.response.out.write(simplejson.dumps([]))
                    return
                                            
                                            
            else:
                consultaNegocios = Negocio.all()
            
            
            if(ultimo != ""):
                negocios = consultaNegocios.with_cursor(ultimo).fetch(80)
            else:
                negocios = consultaNegocios.fetch(80)
            
            cursor = consultaNegocios.cursor()
            
            valoresCache = [negocios,cursor]
            memcache.set(categoria + ultimo,valoresCache)
        else:
            negocios = valoresCache[0]
            cursor = valoresCache[1]
        #negocios = Negocio.all().fetch(100) 
        
        
        result = [{'latitud':negocio.latitud, 
                   'longitud':negocio.longitud,
                   'nombre':negocio.nombre,
                   'direccion':negocio.direccion,
                   'telefono':negocio.telefono,
                   'web':negocio.web,
                   'descripcion':negocio.descripcion,
                   'email':negocio.email,
                   'fax':negocio.fax,
                   'movil':negocio.movil,
                   #'direccionAdicional':negocio.direccionAdicional,
                   'categoria':categoria,
                   'logo':negocio.logoPath,
                   'ultimo':cursor} for negocio in negocios]
        
        self.response.out.write(simplejson.dumps(result))

        

class RecolectorAjax():
    sfsf = "lll"
