import re
import logging
from google.appengine.ext import db
from google.appengine.api import mail

def enviarCorreoUsuarios(destinatario,emisor,titulo,msg, msgHtml):

    mensaje = mail.EmailMessage(sender=emisor,
                                subject=titulo,
                                body=msg)
    if msgHtml:
        mensaje.html = msgHtml 


    mensaje.to = destinatario
    #mensaje.bcc = "aillescas@negocioslalinea.com"
    try:
        mensaje.send()
        return True
    except Exception as e:
        loggin.error("Fallo al enviar correo a " + destinatario)
        loggin.exception(e)
        return False

    
    #if accionPersonalizada:
    #    mensaje.html = temporalHtml
    #    mensaje.body = temporal


