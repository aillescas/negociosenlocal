# coding: utf-8
import os

import webapp2
from django.template.loader import render_to_string
from google.appengine.api import memcache


import logging



import sesionUsuario

from modelos import getInfoLocal
from modelos import Categoria

class PrincipalHandler(webapp2.RequestHandler):
    def get(self):

        logging.debug(str(self.request.cookies))

        infoLocal = getInfoLocal()

        if not infoLocal:
            # Hay que blindar infoLocal para que sólo puedan acceder administradores
            self.redirect("/infoLocal")
            return

        categorias = memcache.get("categorias")
        if not categorias:
            categorias = Categoria.categoriasOrdenadas()
            memcache.set("categorias",categorias)

        
        template_values =  {"title":infoLocal.title, "key":infoLocal.googleMapsKey, "latitudCentral":infoLocal.latitudMapa ,"longitudCentral":infoLocal.longitudMapa , "localidadBusqueda":infoLocal.stringLocalidad, "categorias":categorias}

        template_values.update(infoLocal.getMiscelanea())
        template_values.update(sesionUsuario.recuperarInformacionSesion(self.request))


        path = os.path.join(os.path.dirname(__file__), 'html/index.html')
        
        self.response.out.write(render_to_string(path, template_values))

