# coding: utf-8
import os
import logging 

import sesionUsuario
import webapp2

from django.template.loader import render_to_string
from google.appengine.ext import db
from google.appengine.api import memcache
from django.utils import simplejson

from modelos import Negocio
from modelos import Usuario
from modelos import Categoria
from modelos import getInfoLocal

def asignarIdCliente(usuario):
    indice = memcache.get("indice")
    if(indice):
        indice = indice + 1
    else:
        indice = 0
    memcache.set("indice",indice)

    return str(indice)


class RegistroNegociosHandler(webapp2.RequestHandler):
    def get(self):

        infoLocal = getInfoLocal()

        if not infoLocal:
            # Hay que blindar infoLocal para que sólo puedan acceder administradores
            self.redirect("/infoLocal")
            return


        logging.debug(str(self.request.cookies))
        
        
        categorias = memcache.get("categorias")
        if not categorias:
            categorias = Categoria.categoriasOrdenadas()
            memcache.set("categorias",categorias)

        template_values =  {"title":infoLocal.title, "key":infoLocal.googleMapsKey, "latitudCentral":infoLocal.latitudMapa ,"longitudCentral":infoLocal.longitudMapa , "localidadBusqueda":infoLocal.stringLocalidad, "categorias":categorias }

        template_values.update(infoLocal.getMiscelanea())
        template_values.update(sesionUsuario.recuperarInformacionSesion(self.request))

        path = os.path.join(os.path.dirname(__file__), 'html/registro-negocios.html')
        
        self.response.out.write(render_to_string(path, template_values))



class RegistroNegociosRequestHandler(webapp2.RequestHandler):
    def post(self):
        self.recuperarDatos()
        
    def get(self):
        self.recuperarDatos()


    def recuperarDatos(self):
        #negocio = Negocio()
        #cliente = Usuario()
        #cliente.nombre = self.request.get('propietario')
        #cliente.personalId = "aaaa" 
        #cliente.put()
        nombre = self.request.get('nombre')
        #negocio.cliente = cliente.key()
        direccion = self.request.get('direccion')
        latitud = float(self.request.get('latitud',"0.0"))
        longitud = float(self.request.get('longitud','0.0'))
        categoria = unicode(self.request.get("categoria"))
        
        logging.debug("Categoria: " + unicode(self.request))
        if(categoria):
            categoriaBD = Categoria.all().filter("nombre = ",categoria)
            if(categoriaBD):
                categoria = categoriaBD.get()
                logging.debug("Categoria: " + str(categoria))
        
        negocio = Negocio(nombre=nombre,direccion=direccion,latitud=latitud,longitud=longitud,categoria=categoria)

        negocio.descripcion = self.request.get("descripcion")
        negocio.web = self.request.get('web')
        
        email = self.request.get('email')
        if email:
            negocio.email  = db.Email(email)

        telefono = self.request.get('telefono')
        if telefono:
            negocio.telefono = db.PhoneNumber(telefono)

        movil = self.request.get("movil","")

        if movil:
            negocio.movil = db.PhoneNumber(movil)
        fax = self.request.get("fax","")

        if fax:
            negocio.fax = db.PhoneNumber(fax)

        informacion = self.request.get('h-informacion')
        if informacion:
            negocio.descripcion = informacion
            
        #negocio.categoria = self.request.get("categoria")
        
        #negocio.cliente.put()
        #negocio.personalId = asignarIdNegocio(negocio)
        #Borramos los datos de caché asociados a la categoría
        #Borramos todos los datos de memcache, habrá que hacerlo más selectivo
        memcache.flush_all()
        negocio.put()

        
        logging.debug("Negocio creado: " + negocio.direccion)
        #if not memcache.set ("mensaje", "Negocio creado correctamente", 5):
        #    logging.error("Fallo de memcache")

        self.redirect("/negocios")

        #negocioSerial = [negocio.nombre, negocio.direccion, negocio.web, negocio.telefono, negocio.latitud, negocio.longitud]
        #self.response.out.write(simplejson.dumps(negocioSerial))
        


