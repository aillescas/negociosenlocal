# coding: utf-8
import logging 
import os
import webapp2

from django.template.loader import render_to_string
from google.appengine.ext import db
from google.appengine.api import memcache

import sesionUsuario
import notificacionUsuarios
from modelos import Usuario
from modelos import getInfoLocal
from modelos import UsuarioSinConfirmar


class RegistroUsuariosHandler(webapp2.RequestHandler):

    def get(self):
        infoLocal = getInfoLocal()

        if not infoLocal:
            # Hay que blindar infoLocal para que sólo puedan acceder administradores
            self.redirect("/infoLocal")
            return


        template_values =  {"title":infoLocal.title, "key":infoLocal.googleMapsKey, "latitudCentral":infoLocal.latitudMapa ,"longitudCentral":infoLocal.longitudMapa , "localidadBusqueda":infoLocal.stringLocalidad }
        template_values.update(infoLocal.getMiscelanea())
        template_values.update(sesionUsuario.recuperarInformacionSesion(self.request))

        path = os.path.join(os.path.dirname(__file__), 'html/registro-usuarios.html')
        
        self.response.out.write(render_to_string(path, template_values))


class RegistroUsuariosRequestHandler(webapp2.RequestHandler):
    def post(self):

        infoLocal = getInfoLocal()

        nombre = self.request.get('nombre')
        apellidos = self.request.get('apellidos')
        correo = db.Email(self.request.get('email'))
        password = self.request.get('password')
        
        #Comprobamos que el valor de correo no está ya utilizado
        comprobacionUsuario = Usuario.all().filter("correo = ", db.Email(correo)).get()
        if comprobacionUsuario:
            error = u"El usuario ya está registrado"
            logging.error(error)
            error = u"<p>El usuario ya está registrado</p><br/>"
            error = error + u"<p>Si la dirección de correo le pertenece envíe un correo desde esa misma dirección a: " + infoLocal.mailer + u" indicando su problema. Los correos desde otra dirección de correo que no sea la que indicó en el registro no serán tenidos en cuenta</p>"
            template_values =  {"title":infoLocal.title,"texto":error}
            path = os.path.join(os.path.dirname(__file__), 'html/notificacion.html')
            self.response.out.write(render_to_string(path, template_values))
            return
            
        #usuario.personalId = asignarIdUsuario(usuario)
        usuario = Usuario(nombre=nombre,apellidos=apellidos,correo=correo,password=password)
        usuario.put()

        usuariosin = UsuarioSinConfirmar.generarUsuario(usuario)
        usuariosin.put()
        
        siteName = infoLocal.getMiscelanea()["siteName"]
        
        mensajeHTML = u"<p>Gracias por suscribirse a nuestra página " + siteName + u".</p"
        mensajeHTML = mensajeHTML + u"<br/>"
        mensajeHTML = mensajeHTML + u"<p>Para finalizar el proceso de registro debe visitar la siguiente página: </p>"
        mensajeHTML = mensajeHTML + " www." +  siteName + u"/confirmar?codigo=" + usuariosin.codigoRecuperacion + u"&usuario=" + str(usuario.correo) 


        mensaje = u"Gracias por suscribirse a nuestra página " + siteName + "/n"
        mensaje = mensaje + u"Para finalizar el proceso de registro debe visitar la siguiente dirección: /n"
        mensaje = mensaje + " www." + siteName + u"/confirmar?codigo=" + usuariosin.codigoRecuperacion + u"&usuario=" + str(usuario.correo) + u"/n"

        if not notificacionUsuarios.enviarCorreoUsuarios(str(usuario.correo),infoLocal.mailer,"Confirme su registro en " + siteName,mensaje, mensajeHTML):
            #No se ha podido realizar la notificación, borramos los registros

            logging.error(u"No se ha podido registrar el usuario con los datos: nombre=" + usuario.nombre + u" correo=" + str(usuario.correo))

            error = u"<p>No se ha podido enviar el correo de notificación a la dirección: " + str(usuario.correo) + u". Revise que la dirección de correo es correcta y si el problema persiste envie un correo a: " + infoLocal.mailer + u" indicando su problema."
            usuario.delete()
            usuariosin.delete()

            template_values =  {"title":infoLocal.title,"texto":error}
            template_values.update(sesionUsuario.recuperarInformacionSesion(self.request))

            path = os.path.join(os.path.dirname(__file__), 'html/notificacion.html')
            self.response.out.write(render_to_string(path, template_values))
            return
            
            
        
        notificacion = u"<p>Se ha enviado un mensaje a la dirección de correo: " + str(usuario.correo) + u", siga las instrucciones de ese mensaje para la activación de su usuario.</p><br><p>Si no recibe ningún correo, mire en el buzón o carpeta de Spam. Si sigue sin poder ver el correo contacte con nosotros en la dirección " + infoLocal.mailer + u"</p>"
        template_values = {"title":infoLocal.title, "texto":notificacion}
        path = os.path.join(os.path.dirname(__file__), 'html/notificacion.html')
        self.response.out.write(render_to_string(path, template_values))
        
        #if not memcache.set ("mensaje", "Usuario creado correctamente", 5):
        #    logging.error("Fallo de memcache")



class RegistroUsuariosConfirmacionHandler(webapp2.RequestHandler):
    def get(self):
        self.confirmarUsuario()
    def post(self):
        self.confirmarUsuario()

    def confirmarUsuario(self):
        infoLocal = getInfoLocal()

        if not infoLocal:
            # Hay que blindar infoLocal para que sólo puedan acceder administradores
            self.redirect("/infoLocal")
            return

        codigoRecuperacion = self.request.get("codigo")
        usuarioId = self.request.get("usuario")

        usuario = ""
        logging.debug("codigoRecuperacion: " + str(codigoRecuperacion))
        logging.debug("id Usuario: " + usuarioId)
        consultaUsuarios = UsuarioSinConfirmar.all().filter('codigoRecuperacion = ',codigoRecuperacion)
        logging.debug("Datos de la consulta: " + str(consultaUsuarios.count()))
        for usuarioSin in consultaUsuarios:
            if str(usuarioSin.usuario.correo) == usuarioId:
                usuario = usuarioSin
                break
        
        if not usuario:
            # Habrá que devolver error
            error = u"No se ha encontrado la combinación de código de recuperación y usuario"
            logging.error(error)
            template_values =  {"title":infoLocal.title,"texto":error}
            path = os.path.join(os.path.dirname(__file__), 'html/notificacion.html')
            self.response.out.write(render_to_string(path, template_values))
            return

        usuario.usuario.confirmado = True
        usuario.usuario.put()
        usuarioPagina = usuario.usuario

        usuario.delete()

        texto = "El usuario: " + str(usuario.usuario.correo) + " ha sido activado"
                
        template_values =  {"title":infoLocal.title, "key":infoLocal.googleMapsKey, "latitudCentral":infoLocal.latitudMapa ,"longitudCentral":infoLocal.longitudMapa , "localidadBusqueda":infoLocal.stringLocalidad,"texto":texto}
        #template_values.update(infoLocal.getMiscelanea())

        path = os.path.join(os.path.dirname(__file__), 'html/notificacion.html')

        cookie = sesionUsuario.generarCookieSesion(usuarioPagina)
        self.response.headers.add_header("Set-Cookie",cookie)    
        #self.response.out.write("".join(["Nombre: ",usuarioPagina.nombre, "\n",
        #                                 "Correo: ",usuarioPagina.correo,"\n",
        #                                 "Refer: ",self.request.get('refer',"nada")]))
        
        self.response.out.write(render_to_string(path, template_values))
        


