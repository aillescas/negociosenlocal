# coding: utf-8
import logging
import random
import re
import webapp2

from google.appengine.ext import db
from google.appengine.api import memcache

from django.utils import simplejson

from datetime import datetime
from Cookie import SimpleCookie

from modelos import Sesion
from modelos import Usuario


htmlLogin = u'<a href="/registro">¿Has olvidado la contraseña?</a> | <a href="/registro">¡Regístrate!</a>'
htmlLogin = htmlLogin + u'<form id="login"> '
htmlLogin = htmlLogin + u'<fieldset> '
htmlLogin = htmlLogin + u'<input type="text" name="login-usuario" id="login-usuario" value="Usuario" /> '
htmlLogin = htmlLogin + u'<input type="password" name="login-password" id="login-password" value="Contraseña" /> '
htmlLogin = htmlLogin + u'				<input type="submit" name="login-boton" id="login-boton" value="Entrar" /> '
htmlLogin = htmlLogin + u'				</fieldset> '
htmlLogin = htmlLogin + u' </form>'


def recuperarInformacionSesion(request):
    usuario = recuperarInformacionUsuario(request)
    htmlForm = obtenerHTMLRespuesta(usuario)

    result = {"usuario" : usuario, "htmlForm" : htmlForm}

    return result

def recuperarInformacionUsuario(request):
    idSesion = request.cookies.get("idSesion","")

    #idSesion = re.sub("\"","",idSesion)
    logging.debug("Identificador Sesion: " + idSesion)
    if idSesion:
        usuario = memcache.get(str(idSesion))
        logging.debug("obtenemos de memcache: " + str(usuario))
        if usuario is not None:
            return usuario
        else:
            sesion = Sesion.all().filter('idSesion = ',idSesion).get()
            logging.debug("obtenemos de la base de datos: " + str(sesion))
            if sesion:
                memcache.set(idSesion,sesion.usuario)
                return sesion.usuario
            else:
                return None
    else:
        return None






def generarCookieSesion(usuario):
    "almacena la sesión en la base de datos y genera la cookie para enviar al cliente"
    
    cookie = SimpleCookie()
    idSesion = re.sub(" ","-",str(datetime.now()) + str(random.random()))
    
    cookie["idSesion"] = idSesion
    #cookie["idSesion"]["max-age"]=600
    cookie["idSesion"]["httponly"]=True
    cookie["idSesion"]["path"]='/'

    sesion = Sesion()
    sesion.idSesion = idSesion
    sesion.usuario = usuario
    sesion.put()

    memcache.set(idSesion,usuario)
    logging.debug("Cookie Generada: " + cookie["idSesion"].output(header='').lstrip())
    logging.debug("Cookie Generada2: " +  re.sub("\"","",cookie["idSesion"].output(header='').lstrip()))
    return re.sub("\"","",cookie["idSesion"].output(header='').lstrip())
        

def logout(request):
    "elimina los datos de la sesion y genera una cookie con max-age=0 para su eliminación en el cliente"
    idSesion = request.cookies.get("idSesion","")
    sesion = Sesion.all().filter("idSesion = ",idSesion).get()
    if sesion:
        sesion.delete()

    memcache.delete(idSesion)

    cookie = SimpleCookie()
    cookie["idSesion"] = idSesion
    cookie["idSesion"]["httponly"]=True
    cookie["idSesion"]["path"]='/'
    cookie["idSesion"]["Expires"] = 'Thu, 01-Jan-1970 00:00:00 GMT'

    return re.sub("\"","",cookie["idSesion"].output(header='').lstrip())
        
def obtenerCodigoError(usuario,idUsuario,password):
    error = ""
    if usuario:
        if usuario.password == password:
            if not usuario.confirmado:
              error = u"Usuario no confirmado, consulte su correo electrónico"
        else:
            error = u"El password no coincide con el usuario proporcionado"
    else:
        error = u"El identificador de usuario " + idUsuario + u" no se encuentra registrado"

    return error
        

def obtenerHTMLRespuesta(usuario):
    if usuario:
        return u"Bienvenido/a: " + usuario.nombre + u'<form id="login"><input type="submit" name="logout-boton" id="logout-boton" value="salir"/></form>'
    else:
        return htmlLogin

class UsuarioAjaxHandler(webapp2.RequestHandler):

    def post(self):
        logging.debug("Inicio UsuarioAjaxHandler")
        usuario = self.request.get('usuario')
        password = self.request.get('password')

        logging.debug("usuario: " + usuario)
        logging.debug("password: " + password)
        result = []

        if usuario and password:
            #Es una solicitud de login, buscamos al usuario en la base de datos
            logging.debug("Solicitud de login")
            usuarioBd = Usuario.all().filter("correo = ", db.Email(usuario)).get()
            error = obtenerCodigoError(usuarioBd,usuario,password)
            html = obtenerHTMLRespuesta(usuarioBd)
            logging.debug("resultado= " + (error or "ok"))
            if not error:
                cookie = generarCookieSesion(usuarioBd)
                self.response.headers.add_header("Set-Cookie",cookie)
            result = [{"error" : error,
                       "htmlForm" : html}]

        else:
            #Es una solicitud de logout, recuperamos los datos de sesión y procedemos a eliminarla
            logging.debug("Solicitud de logout")
            cookie = logout(self.request)
            self.response.headers.add_header("Set-Cookie",cookie)    

            result = [{"error" : "",
                       "htmlForm" : htmlLogin}]

        logging.debug("resultado final : " + str(result))
        self.response.out.write(simplejson.dumps(result))            
            
        return 
            

