var mapa = null;
var geocoder = null;

var indiceLocal;
var indicePeticion;
var resultado;
var encontradas = 0;
var descargadas = 0;
var ultimo="";  

var marcas;
var infowindow;



function getMyOptions(latitudCentral, longitudCentral){
    var myOptions = {
	center: new google.maps.LatLng(latitudCentral, longitudCentral),
	zoom: 14,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles:[ 
            { 
                featureType: "poi", 
                elementType: "labels", 
                stylers: [{ visibility: "off" }] 
            } 
        ]
    };
    
    return myOptions;
}


function inicializarMapaRegistro(latitudCentral, longitudCentral,idMapa,idLatitud,idLongitud){
    mapa = new google.maps.Map(document.getElementById(idMapa), getMyOptions(latitudCentral, longitudCentral));
    if(geocoder == null)
	geocoder = new google.maps.Geocoder();
    var punto = new google.maps.LatLng(latitudCentral, longitudCentral, true);
    mapa.panTo(punto);


    google.maps.event.addListener(mapa,"click", 
				   function(mouseEvent){
				       clearOverlays();
		
				       
				       marcador = new google.maps.Marker({
					   position:mouseEvent.latLng,
				           map:mapa
				       });
				       var latitud = mouseEvent.latLng.lat();
				       var longitud = mouseEvent.latLng.lng();
				       $(idLatitud).val(latitud);
				       $(idLongitud).val(longitud);

				       marcas[latitud + "-" + longitud] = marcador
							 
				   });
}
	

function inicializarMapaBusqueda(latitudCentral,longitudCentral,idMapa) {
    mapa = new google.maps.Map(document.getElementById(idMapa), getMyOptions(latitudCentral, longitudCentral));
    
    if(geocoder == null)
	geocoder = new google.maps.Geocoder();	
    infowindow = new google.maps.InfoWindow({content: "....Inicializando"});

}


function showAddressRegistro(idDireccion,localidadBusqueda,idLatitud,idLongitud) {
    var address = $(idDireccion).val();
    address = address + localidadBusqueda;
    clearOverlays();
    if (geocoder) {
        geocoder.geocode({address:address},
                         function(resultado, status) {
                             if (status != google.maps.GeocoderStatus.OK) {
                                 alert(address + ": dirección no encontrada. Seleccione su dirección en el mapa");
                             } 
                             else {
	                         var zoomLevel = 19;
	                         var point = resultado[0].geometry.location;
				 
				 $(idLatitud).val(point.lat());
				 $(idLongitud).val(point.lng());
				 
                                 mapa.setMapTypeId(google.maps.MapTypeId.HYBRID);
                                 mapa.panTo(point);
	                         mapa.setZoom(zoomLevel);

				 var marcador = new google.maps.Marker({
				     position:point,
				     map:mapa,
				     title:address
				 });
				 marcas[point.lat() + "-" + point.lng()] = marcador
                             }
                         }   
                        );
	
    }
}   

	  



/**
 * Mostrar y ocultar el formulario de busqueda
 */
function mostrarSugerencias(e) {
    e.preventDefault();
    
    visible = $(".form-sugerencia").css("display");
    estado_animacion = $(".form-sugerencia").is(":animated");
    
    if (visible == "none") 	
	$(".enviar-sugerencia").html("Cerrar sugerencia");
    else{
	$(".enviar-sugerencia").html("Enviar sugerencia");
	//
    }
		
    // Si ya se está animando, no hacemos nada
    if (!estado_animacion)
	$(".form-sugerencia").slideToggle();
};

function mostrarInfoEmpresa(empresa) {

    //Actualizamos la información de la empresa
    $("#info-negocios-cabecera").html(empresa.nombre);
    $("#info-negocios-direccion").html(empresa.direccion);
    $("#info-negocios-descripcion").html(empresa.descripcion);
    if(empresa.logo && empresa.logo != ""){
	$("#info-negocios-logo").attr("src",empresa.logo);
    }
    $("#info-negocios-logo").attr("title",empresa.nombre);
    $("#info-negocios-logo").attr("alt",empresa.nombre);

    if(empresa.web && empresa.web != ""){
	var dirWeb;
	if(empresa.web.slice(0,7) == "http://")
	    dirWeb = empresa.web;
	else
	    dirWeb = "http://" + empresa.web;

	$("#info-negocios-web").attr("href",dirWeb);
	$("#info-negocios-web").html(empresa.web);
    }
    else{
	$("#info-negocios-web").attr("href","#info-negocios-cabecera");
	$("#info-negocios-web").html("No disponible");
    }

    $("#info-negocios-web").attr("title","Ir al sitio web de " + empresa.nombre);

    if(empresa.telefono)
	$("#info-negocios-tlf").html(empresa.telefono);
    else
	$("#info-negocios-tlf").html("No disponible");
 
    if(empresa.email && empresa.email != ""){
	$("#info-negocios-mail").attr("href","mailto:" + empresa.email);
	$("#info-negocios-mail").html(empresa.email);
    }
    else{
	$("#info-negocios-mail").attr("href","#info-negocios-cabecera");
	$("#info-negocios-mail").html("No disponible");
    }	
    $("#info-negocios-mail").attr("title","Contacte con " + empresa.nombre);    

    estado_animacion = $(".info-negocio").is(":animated");
    
    // Si ya se está animando, no hacemos nada
    if (!estado_animacion)
	$(".info-negocio").slideToggle();
}

function ocultarInfoEmpresa(){
    $(".form-sugerencia").hide();

    $(".info-negocio").hide();

    return true;
}

function clearOverlays(){
    for (clave in marcas){
	//Borramos el marcador eliminándolo del mapa
	marcas[clave].setMap(null);
	delete marcas[clave];
    }

    marcas = null;
    marcas = {};
    //Eliminamos la memoria
    return true;
};

function mostrarNegocios(datos,borrar){
    if(!mapa)
        return;

    if(borrar) 
	clearOverlays();
    var r= '';
    var marcador;
    var punto;
    var negocio;
    var longitud = datos.length

    for (var i = 0; i < longitud; i++)
    { 

        negocio = datos[i]
	//alert(negocio.latitud + "-" + negocio.longitud)
	// Desplazamos la marca ligeramente sobre su posición inicial para que no coincidan
	if(marcas[negocio.latitud + "-" + negocio.longitud]){
	    negocio.latitud = negocio.latitud + ((Math.floor(Math.random() * 5000)) / 100000000);
	    negocio.longitud = negocio.longitud + ((Math.floor(Math.random() * 5000)) / 100000000);
	}

        punto = new google.maps.LatLng(parseFloat(negocio.latitud), parseFloat(negocio.longitud), true);
	
        marcador = new google.maps.Marker({
	    position:punto,
	    map:mapa,
	    title:negocio.nombre,
	    empresa: negocio,
	    html:'<div class="info-map"><div class="title">' + negocio.nombre + '</div><div>' + negocio.direccion +  ' <br>' + negocio.descripcion + '</div></div>'
	});

        google.maps.event.addListener(marcador, "click", function () {

            //alert(this.html);
	    if(infowindow){ 
		infowindow.close();
		ocultarInfoEmpresa();
	    }

	    infowindow = new google.maps.InfoWindow({

                content: this.html

            });


            infowindow.open(mapa, this);
	    mostrarInfoEmpresa(this.empresa);
	    setTimeout('$(".enviar-sugerencia").click(mostrarSugerencias)',500);

	    google.maps.event.addListener(infowindow, "closeclick", ocultarInfoEmpresa);
        });

	marcas[negocio.latitud + "-" + negocio.longitud] = marcador
	


    }; 
    
    //
};

function borradoAjax(datos){

    if(datos && datos.length > 0){
	entidad = datos[0].entidad
	cuenta  = datos[0].cuenta
	if(cuenta == "1000"){
	    cuenta = "1000+"
        }
	$("div." + entidad).text("Contador: " + cuenta)	

        parametros = new Object();
        parametros.url = "/admin/borrar";
        parametros.type = "GET";
        parametros.dataType = "json";
      
        datosEnviar =  new Object();
        datosEnviar.entidad = entidad;
        parametros.data = datosEnviar;
	parametros.success = borradoAjax
	
    	if(datos[0].cuenta != 0){
	    $.ajax(parametros);
	    //setTimeout("$.ajax(parametros)",10);
	}

    }
}
function mostrarCategorias(categoria){
    //alert(categoria)
    //borramos los datos anteriores y limpiamos el mapa
    clearOverlays();
    ocultarInfoEmpresa();

    var parametros = new Object();
    parametros.url = "/ajax";
    parametros.type = "GET";
    parametros.dataType = "json";
    parametros.contentType = "application/json; charset=utf-8"
    
    datos =  new Object();
    datos.categoria = categoria;
    parametros.data = datos;
    parametros.success = comunicacionAjax;
    parametros.error = falloCargarCategorias;
    
    $.ajax(parametros);

    mostrarFiltroCarga();
}


function mostrarFiltroCarga(){
    var mapa = $('#mapa');
    var posicion = mapa.position();
    var altura   = mapa.height();
    var ancho    = mapa.width();
    
    var filtroCarga = $('#cargando');

    filtroCarga.height(altura);
    filtroCarga.width(ancho);
    filtroCarga.css('top',posicion.top);
    filtroCarga.css('left',posicion.left);
    filtroCarga.show();

    
}

function falloCargarCategorias(){
    //alert("fallo");
    $('#cargando').hide()
};

function comunicacionAjax(datos){
    
    //alert("categoria: ");

    if((datos && datos.length == 0) || !datos){
	//alert("fin de la carga de datos");
	$('#cargando').hide();
	return;
    }
    var ultimo = ""
    var categoria = ""
    if(datos && datos.length > 0){
	// Acabamos de recibir los datos de una petición, los mostramos en el mapa
	mostrarNegocios(datos,false);
	ultimo = datos[datos.length - 1].ultimo
	categoria = datos[datos.length - 1].categoria
    }
    
    
    // Iniciamos la petición de datos
    var parametros = new Object();
    parametros.url = "/ajax";
    parametros.type = "GET";
    parametros.dataType = "json";
    parametros.contentType = "application/json; charset=utf-8"
    
    datos =  new Object();
    datos.categoria = categoria;
    datos.ultimo = ultimo;
    parametros.data = datos;
    parametros.success = comunicacionAjax;
    parametros.error = falloCargarCategorias;
    
    $.ajax(parametros);
    //setTimeout("$.getJSON('ajax?categoria=" + categoria + "&ultimo="+ultimo + "', {}, comunicacionAjax)",10);	


}





function localizarNegocios(datos){
    if(!mapa || !geocoder) return;
    
    var negocio;
    var longitud = datos.length;
    
    var marcador;
    
    indiceLocal = 0;
    indicePeticion = 0;
    resultado = new Array();
    
    descargadas += longitud;
    document.getElementById("descargadas").value = descargadas;
    //alert("Recibidos " + longitud + " resultados");
    for (var i = 0; i < longitud; i++)
    {
        negocio = datos[i];
	
        resultado[i] = new Object();
        resultado[i].nombre = negocio.nombre;
        resultado[i].direccion = negocio.direccion;
        resultado[i].descripcion = negocio.descripcion;
        resultado[i].direccionAdicional = negocio.direccionAdicional;
    }
    // Con esta llamada se realiza una llamada al API de Google Maps para obtener la localización de la dirección, eventualmente se prescindirá de ella
    //if(longitud > 0){			  
    //((    lanzarPeticionLocalizacion();
    //} 
    //else alert("Fin del proceso");
    
    
    
}

function comprobarPeticion(){
    if(indiceLocal < resultado.length){
	// Aún quedan peticiones por hacer así que comprobamos el estado de las peticiones y si es necesario llamamos a la función correspondiente
       if(indiceLocal == indicePeticion){
           lanzarPeticionLocalizacion();
       }
	
    }
    else{
        // No hay nada que hacer, se han terminado las peticiones salimos de la función y almacenamos el último valor para futuras llamadas
	ultimo = resultado[resultado.length - 1].nombre;	      
	//alert("Fin encontradas: " + encontradas + " Último: " + ultimo)
	//document.getElementById("encontradas").value = encontradas;
	
	setTimeout("$.getJSON('ajax?ultimo="+ultimo + "', {}, localizarNegocios)",10);
	return;
    }
    
}

function lanzarPeticionLocalizacion(){
    // Vamos a tener una única petición asincrona activa en cada momento, para poder casar
    // los datos de la respuesta con los de la petición, porque si hay varias peticiones
    // activas a la vez, nada nos asegura que las respuestas vengan en el mismo orden en 
    // el que las hicimos
    
      // Realizamos una petición con los datos que hay en resultado[indicePeticion]
    // Incrementamos indiceLocal para indicar que hay una petición en marcha
    indiceLocal += 1 
    //      geocoder.getLatLng(resultado[indicePeticion].direccion + ", La Linea de la Concepcion",
    //			  function(point){
    //		             var texto;
    //			     if(!point){
    //
    //                             }
    //			     else{
    //			        marcador = new GMarker(point);
    //			        map.addOverlay(marcador);
    //			        marcador.bindInfoWindowHtml(resultado[indicePeticion].nombre +// "<br>" + resultado[indicePeticion].direccion + "<br>");
    //                                encontradas += 1;
    //                             }
    //		             texto = document.getElementById("texto");
    //		             texto.value = texto.value + "\n" + resultado[indicePeticion].direccion + "----" + resultado[indicePeticion].nombre + "----" + 
    //		                           point + "----" + resultado[indicePeticion].descripcion + "----" + resultado[indicePeticion].direccionAdicional;
    
    // Una vez terminada la petición incrementamos el puntero indicePetición para indicar que está resuelta;
    //                             indicePeticion += 1;
    //		             setTimeout("comprobarPeticion()",250);
    //                          });
    
    geocoder.getLocations(resultado[indicePeticion].direccion + ", La Linea de la Concepcion",	      
			  function(datosJson){
		              var texto;
                             texto = document.getElementById("log");
                              var point = false;
			      if(datosJson.Status.code != 200){
				  texto.value += "\n" + datosJson.Status.code + " " + datosJson.name 
				  
				  //for(var fieldname in datosJson){
		                  //    texto.value += " " + fieldname;
				  //}

				  
                              }
			      else{
                                  for (var i = 0; i < datosJson.Placemark.length; i++){
                                      texto.value += "\n" + datosJson.Placemark[i].address;
			          }
		                  point = new GLatLng(datosJson.Placemark[0].Point.coordinates[1],datosJson.Placemark[0].Point.coordinates[0]);
			          marcador = new GMarker(point);
			          mapa.addOverlay(marcador);
			          marcador.bindInfoWindowHtml(resultado[indicePeticion].nombre + "<br>" + resultado[indicePeticion].direccion + "<br>");
                                  encontradas += 1;
                              }
		              texto = document.getElementById("texto");
		              texto.value = texto.value + "\n" + resultado[indicePeticion].direccion + "----" + resultado[indicePeticion].nombre + 
                                  "----" + point + "----" + resultado[indicePeticion].descripcion + "----" + resultado[indicePeticion].direccionAdicional;
			      
                              // Una vez terminada la petición incrementamos el puntero indicePetición para indicar que está resuelta;
                              indicePeticion += 1;
		              setTimeout("comprobarPeticion()",250);
                          });
    
}


/*function showAddress(address) {
    if (geocoder) {
        geocoder.getLatLng(
            address,
            function(point) {
		if (!point) {
		    alert(address + " not found");
		} else {
		    zoomlevel = mapa.getZoom();
		    mapa.setCenter(point, zoomlevel);
		    alert('latitud: ' + point.lat() + ' longitud: ' + point.lng());
		    var marker = new GMarker(point);
		    map.addOverlay(marker);
		    marker.bindInfoWindowHtml(address);
		}
            }
        );
      }
}*/
