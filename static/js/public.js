
$(document).ready(function() {



    function loginLogout(event){
	event.preventDefault();
	
	var usuario = $("#login-usuario");
	var password = $("#login-password");
	
	var parametros = new Object();
	parametros.url = "/gestionUsuario";
	parametros.type = "POST";
	parametros.dataType = "json";
	if(usuario.length == 1 && password.length == 1){
	    var datosEnviar = new Object();
	    datosEnviar.usuario = usuario.val();
	    datosEnviar.password = password.val();
	    parametros.data = datosEnviar
	}

	parametros.success = gestionUsuario;
	notificacionMensaje("Conectando con el servidor",false);
	$.ajax(parametros);
    }

    function gestionUsuario(datosLogin){
	var loginForm = $("div.superior");
	
	loginForm.html(datosLogin[0].htmlForm);

	$("#login").submit(loginLogout);
	
	var error = datosLogin[0].error;
	notificacionMensaje(error, error != "");
    }

    $("#login").submit(loginLogout);
    

});


function notificacionMensaje(mensaje, error){
    var notificacionTag = $("div.notificacion");
    if(notificacionTag.length > 0){
	notificacion = "";
	if(error)
	    notificacion = "<span class=\"mensajeError\"> ";
	else
	    notificacion = "<span class=\"mensajeInfo\"> ";
	notificacion = notificacion + mensaje + "</span>";

	notificacionTag.html(notificacion);
	notificacionTag[0].scrollIntoView(true);
    }
    else
	alert(mensaje);
}
