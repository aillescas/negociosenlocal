function validarNombre(nombreId, notificacion)
{
    var nombre = $("input#" + nombreId);
    
    if(nombre.length == 0){
	notificacion("No se ha encontrado el campo Nombre, consulte con el administrador",true);
	return false;
    }
    
    if($.trim(nombre.val()).length > 0)
	return true;
    else{
	notificacion("El nombre no puede estar en blanco",true);
	return false;
    }
}

function validarEmail(emailId, notificacion)
{
    var email = $("input#" + emailId);
    
    if(email.length == 0){
	notificacion("No se ha encontrado el campo Email, consulte con el administrador",true);
	return false;
    }
    
    valorEmail = $.trim(email.val());
    if(valorEmail.length == 0){
	notificacion("El Email no puede estar en blanco",true);
	return false;
    }
    
    if(valorEmail.toUpperCase().match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/)){
	return true;
    }
    else{
	notificacion("El formato del email no es correcto",true);
	return false;
    }
    
    return true;
}

function validarTelefono(telefono)
{
    return true;
}

function validarPassword(identificador1, identificador2, notificacion)
{
    var id1 = $("input#" + identificador1);
    var id2 = $("input#" + identificador2);
    
    
    
    if (id1.length > 0 && id2.length > 0){
	if($.trim(id1.val()).length < 6){
	    notificacion("El password no puede ser inferior a 6 caracteres", true);
	    return false;
	}
	
	result = id1.val() == id2.val();
	if(!result)
	    notificacion("El password no coincide",true);
	return result;
    }

    notificacion("No se ha encontrado el campo password, consulte con el administrador",true);
    return false;
}


function validarTextoObligatorio(identificador, campo, notificacion)
{
    var elemento = $("input#" + identificador);
    
    if(elemento.length == 0){
	notificacion("No se ha encontrado el campo " + campo + ", consulte con el administrador",true);
	return false;
    }
    
    if($.trim(elemento.val()).length > 0)
	return true;
    else{
	notificacion(campo + " no puede estar en blanco",true);
	return false;
    }
}
