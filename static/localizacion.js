var map = null;
var geocoder = null;

var indiceLocal;
var indicePeticion;
var resultado;
var encontradas = 0;
var descargadas = 0;
var ultimo="";  

// HAY QUE RETOCAR TODO PARA SEPARAR LA FUNCIONALIDAD DE RECUPERAR LAS DIRECCIONES DE BASE DE DATOS Y POR OTRO LADO LA CONSULTA A LA LATITUD-LONGITUD DE DIRECCIONES DADAS,
// PARA GENERAR ESTOS DATOS EN NUEVAS INCORPORACIONES.



function comunicacionAjax(datos){

    if(datos){
	// Acabamos de recibir los datos de una petición, los mostramos en el mapa
	mostrarNegocios(datos);
	ultimo = datos[datos.length - 1].nombre
    }
    
    // Iniciamos la petición de datos
    setTimeout("$.getJSON('ajax?ultimo="+ultimo + "', {}, comunicacionAjax)",10);	


}





function localizarNegocios(datos){
    if(!map || !geocoder) return;
    
    var negocio;
    var longitud = datos.length;
    
    var marcador;
    
    indiceLocal = 0;
    indicePeticion = 0;
    resultado = new Array();
    
    descargadas += longitud;
    document.getElementById("descargadas").value = descargadas;
    //alert("Recibidos " + longitud + " resultados");
    for (var i = 0; i < longitud; i++)
    {
        negocio = datos[i];
	
        resultado[i] = new Object();
        resultado[i].nombre = negocio.nombre;
        resultado[i].direccion = negocio.direccion;
        resultado[i].descripcion = negocio.descripcion;
        resultado[i].direccionAdicional = negocio.direccionAdicional;
    }
    // Con esta llamada se realiza una llamada al API de Google Maps para obtener la localización de la dirección, eventualmente se prescindirá de ella
    //if(longitud > 0){			  
    //((    lanzarPeticionLocalizacion();
    //} 
    else alert("Fin del proceso");
    
    
    
}

function comprobarPeticion(){
    if(indiceLocal < resultado.length){
	// Aún quedan peticiones por hacer así que comprobamos el estado de las peticiones y si es necesario llamamos a la función correspondiente
       if(indiceLocal == indicePeticion){
           lanzarPeticionLocalizacion();
       }
	
    }
    else{
        // No hay nada que hacer, se han terminado las peticiones salimos de la función y almacenamos el último valor para futuras llamadas
	ultimo = resultado[resultado.length - 1].nombre;	      
	//alert("Fin encontradas: " + encontradas + " Último: " + ultimo)
	//document.getElementById("encontradas").value = encontradas;
	
	setTimeout("$.getJSON('ajax?ultimo="+ultimo + "', {}, localizarNegocios)",10);
	return;
    }
    
}

function lanzarPeticionLocalizacion(){
    // Vamos a tener una única petición asincrona activa en cada momento, para poder casar
    // los datos de la respuesta con los de la petición, porque si hay varias peticiones
    // activas a la vez, nada nos asegura que las respuestas vengan en el mismo orden en 
    // el que las hicimos
    
      // Realizamos una petición con los datos que hay en resultado[indicePeticion]
    // Incrementamos indiceLocal para indicar que hay una petición en marcha
    indiceLocal += 1 
    //      geocoder.getLatLng(resultado[indicePeticion].direccion + ", La Linea de la Concepcion",
    //			  function(point){
    //		             var texto;
    //			     if(!point){
    //
    //                             }
    //			     else{
    //			        marcador = new GMarker(point);
    //			        map.addOverlay(marcador);
    //			        marcador.bindInfoWindowHtml(resultado[indicePeticion].nombre +// "<br>" + resultado[indicePeticion].direccion + "<br>");
    //                                encontradas += 1;
    //                             }
    //		             texto = document.getElementById("texto");
    //		             texto.value = texto.value + "\n" + resultado[indicePeticion].direccion + "----" + resultado[indicePeticion].nombre + "----" + 
    //		                           point + "----" + resultado[indicePeticion].descripcion + "----" + resultado[indicePeticion].direccionAdicional;
    
    // Una vez terminada la petición incrementamos el puntero indicePetición para indicar que está resuelta;
    //                             indicePeticion += 1;
    //		             setTimeout("comprobarPeticion()",250);
    //                          });
    
    geocoder.getLocations(resultado[indicePeticion].direccion + ", La Linea de la Concepcion",	      
			  function(datosJson){
		              var texto;
                             texto = document.getElementById("log");
                              var point = false;
			      if(datosJson.Status.code != 200){
				  texto.value += "\n" + datosJson.Status.code + " " + datosJson.name 
				  
				  //for(var fieldname in datosJson){
		                  //    texto.value += " " + fieldname;
				  //}

				  
                              }
			      else{
                                  for (var i = 0; i < datosJson.Placemark.length; i++){
                                      texto.value += "\n" + datosJson.Placemark[i].address;
			          }
		                  point = new GLatLng(datosJson.Placemark[0].Point.coordinates[1],datosJson.Placemark[0].Point.coordinates[0]);
			          marcador = new GMarker(point);
			          map.addOverlay(marcador);
			          marcador.bindInfoWindowHtml(resultado[indicePeticion].nombre + "<br>" + resultado[indicePeticion].direccion + "<br>");
                                  encontradas += 1;
                              }
		              texto = document.getElementById("texto");
		              texto.value = texto.value + "\n" + resultado[indicePeticion].direccion + "----" + resultado[indicePeticion].nombre + 
                                  "----" + point + "----" + resultado[indicePeticion].descripcion + "----" + resultado[indicePeticion].direccionAdicional;
			      
                              // Una vez terminada la petición incrementamos el puntero indicePetición para indicar que está resuelta;
                              indicePeticion += 1;
		              setTimeout("comprobarPeticion()",250);
                          });
    
}

function mostrarNegocios(datos){
      if(!map){
          return;
      }
    map.clearOverlays();
    var r= '';
    var marcador;
    var punto;
    var negocio;
    var longitud = datos.length
    for (var i = 0; i < longitud; i++)
    { 
        negocio = datos[i]
        punto = new GLatLng(parseFloat(negocio.latitud), parseFloat(negocio.longitud), true);
        marcador = new GMarker(punto);
        map.addOverlay(marcador);
	marcador.bindInfoWindowHtml(negocio.nombre + "<br>" + negocio.direccion +  " " + negocio.direccionAdicional + "<br>" + negocio.descripcion + "<br>");
    }; 
    
    //
};

function showAddress(address) {
    if (geocoder) {
        geocoder.getLatLng(
            address,
            function(point) {
		if (!point) {
		    alert(address + " not found");
		} else {
		    zoomlevel = map.getZoom();
		    map.setCenter(point, zoomlevel);
		    alert('latitud: ' + point.lat() + ' longitud: ' + point.lng());
		    var marker = new GMarker(point);
		    map.addOverlay(marker);
		    marker.bindInfoWindowHtml(address);
		}
            }
        );
      }
}
